﻿namespace RecordTheBeat.Data
{
    public class HPValue
    {
        public int TimeMillis { get; set; }
        public float Value { get; set; }
    }
}
