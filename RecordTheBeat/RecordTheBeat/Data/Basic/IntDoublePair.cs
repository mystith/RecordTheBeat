﻿namespace RecordTheBeat.Data.Basic
{
    public class IntDoublePair
    {
        public int X { get; set; }
        public double Y { get; set; }

        public IntDoublePair(int A, double B)
        {
            X = A;
            Y = B;
        }
    }
}
