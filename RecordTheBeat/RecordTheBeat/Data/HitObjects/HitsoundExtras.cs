﻿namespace RecordTheBeat.Data.HitObjects
{
    public class HitsoundExtras
    {
        public int SampleSet { get; set; }
        public int AdditionSet { get; set; }
        public int CustomIndex { get; set; }
        public int SampleVolume { get; set; }
        public string Filename { get; set; }
    }
}
