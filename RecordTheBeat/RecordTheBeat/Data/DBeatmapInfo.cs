﻿namespace RecordTheBeat.Data
{
    public class DBeatmapInfo
    {
        public int Size { get; set; }
        public string MD5Hash { get; set; }
        public string OSUFile { get; set; }
        public string FolderName { get; set; }
    }
}
